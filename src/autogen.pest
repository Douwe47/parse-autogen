// SPDX-License-Identifier: MIT

magic = { "AutoGen Definitions options;" }

WHITESPACE = _{ " " | NEWLINE }
COMMENT = _{
      "//" ~ (!NEWLINE ~ ANY)*
    | "/*" ~ (!"*/" ~ ANY)* ~ "*/"
    | "#" ~ (!NEWLINE ~ ANY)*
}

ident = @{ (ASCII_ALPHANUMERIC | "_" | "-")* }

string = ${ "\"" ~ doublequoted_inner ~ "\"" | "'" ~ quoted_inner ~ "'" }
doublequoted_inner = @{ doublequoted_char* }
doublequoted_char = {
      !("\"" | "\\") ~ ANY
    | "\\" ~ ("\"" | "\\" | "n" | "t")
}
quoted_inner = @{ quoted_char* }
quoted_char = {
      !("\'" | "\\") ~ ANY
    | "\\" ~ ("\'" | "\\" | "n" | "t")
}

heredoc_marker = @{ (ASCII_ALPHA | "_")* }

heredoc_interior = ${ ((!(PEEK | NEWLINE) ~ ANY)* ~ NEWLINE)* }

heredoc = @{
      "<<-" ~ PUSH(heredoc_marker) ~ NEWLINE
      ~ heredoc_interior
      ~ POP
}

field = {
      ident ~ ";"
    | ident ~ "=" ~ ident ~ ";"
    | ident ~ "=" ~ string* ~ ";"
    | ident ~ "=" ~ heredoc ~ ";"
}

copyright = { "copyright" ~ "=" ~ "{" ~ field* ~ "}" ~ ";" }
opt = { "flag" ~ "=" ~ "{" ~ field* ~ "}" ~ ";" }
doc = { "doc-section" ~ "=" ~ "{" ~ field* ~ "}" ~ ";" }

file = {
      SOI
      ~ magic
      ~ field*
      ~ copyright?
      ~ (opt | doc)*
      ~ EOI
}
