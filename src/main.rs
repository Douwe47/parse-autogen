// SPDX-License-Identifier: MIT

extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::iterators::Pair;
use pest::Parser;
use serde::Serialize;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::ErrorKind;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Parser)]
#[grammar = "autogen.pest"]
pub struct AutoGenParser;

#[derive(StructOpt)]
struct Opt {
    file: PathBuf,
}

#[derive(Serialize, Clone, Default)]
struct ToolDesc {
    #[serde(rename = "name")]
    name: String,
    #[serde(rename = "title")]
    title: String,
    #[serde(rename = "description")]
    description: String,
    #[serde(rename = "short-usage")]
    short_usage: String,
    #[serde(rename = "argument")]
    argument: String,
    #[serde(rename = "reorder-arguments", skip_serializing_if = "Option::is_none")]
    reorder_arguments: Option<bool>,
}

#[derive(Serialize, Clone)]
enum ArgumentType {
    #[serde(rename = "string")]
    String,
    #[serde(rename = "number")]
    Number,
    #[serde(rename = "file")]
    File,
    #[serde(rename = "keyword")]
    Keyword,
}

impl From<&str> for ArgumentType {
    fn from(s: &str) -> Self {
        match s {
            "string" => ArgumentType::String,
            "number" => ArgumentType::Number,
            "file" => ArgumentType::File,
            "keyword" => ArgumentType::Keyword,
            _ => unreachable!(),
        }
    }
}

#[derive(Serialize, Clone)]
struct Range<T> {
    #[serde(skip_serializing_if = "Option::is_none")]
    min: Option<T>,
    #[serde(skip_serializing_if = "Option::is_none")]
    max: Option<T>,
}

#[derive(Serialize, Clone)]
struct OptionDesc {
    #[serde(rename = "long-option")]
    long_option: String,
    #[serde(rename = "short-option", skip_serializing_if = "Option::is_none")]
    short_option: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    detail: Option<String>,
    #[serde(rename = "argument-optional", skip_serializing_if = "Option::is_none")]
    argument_optional: Option<bool>,
    #[serde(rename = "file-exists", skip_serializing_if = "Option::is_none")]
    file_exists: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    deprecated: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    aliases: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    conflicts: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    requires: Option<Vec<String>>,
    #[serde(rename = "argument-range", skip_serializing_if = "Option::is_none")]
    argument_range: Option<Range<i32>>,
    #[serde(rename = "argument-type", skip_serializing_if = "Option::is_none")]
    argument_type: Option<ArgumentType>,
    #[serde(rename = "argument-name", skip_serializing_if = "Option::is_none")]
    argument_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    multiple: Option<bool>,
    #[serde(rename = "occur-range", skip_serializing_if = "Option::is_none")]
    occur_range: Option<Range<usize>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    enabled: Option<bool>,
    #[serde(rename = "disable-prefix", skip_serializing_if = "Option::is_none")]
    disable_prefix: Option<String>,
    #[serde(rename = "enable-prefix", skip_serializing_if = "Option::is_none")]
    enable_prefix: Option<String>,
}

#[derive(Serialize, Clone, Default)]
struct SectionDesc {
    #[serde(skip_serializing_if = "Option::is_none")]
    id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    description: Option<String>,
    options: Vec<OptionDesc>,
}

#[derive(Serialize, Clone)]
struct Desc {
    #[serde(rename = "format-version")]
    format_version: String,
    tool: ToolDesc,
    sections: Vec<SectionDesc>,
}

fn rename_key(map: &mut HashMap<String, String>, old: &str, new: &str) {
    if let Some(v) = map.remove(old) {
        map.insert(new.to_string(), v);
    }
}

fn parse_string(anystring: Pair<Rule>) -> String {
    match anystring.as_rule() {
        Rule::string => {
            let mut inner = anystring.into_inner();
            let value = inner.next().unwrap();
            match value.as_rule() {
                Rule::doublequoted_inner => value
                    .as_str()
                    .replace("\\n", "\n")
                    .replace("\\t", "\t")
                    .replace("\\\"", "\"")
                    .replace("\\\\", "\\"),
                Rule::quoted_inner => value
                    .as_str()
                    .replace("\\n", "\n")
                    .replace("\\t", "\t")
                    .replace("\\'", "'")
                    .replace("\\\\", "\\"),
                _ => unreachable!(),
            }
        }
        Rule::heredoc => {
            let mut inner = anystring.into_inner();
            inner.next().unwrap().as_str().to_string()
        }
        Rule::ident => anystring.as_str().to_string(),
        _ => unreachable!(),
    }
}

fn parse_field(field: Pair<Rule>) -> (String, String) {
    let mut field_inner = field.into_inner();
    let mut buffer = String::new();
    let name = field_inner.next().unwrap().as_str();
    for value in field_inner {
        buffer.push_str(&parse_string(value))
    }
    (name.to_lowercase().replace("_", "-"), buffer)
}

fn parse_fields(fields: Pair<Rule>) -> HashMap<String, String> {
    let mut result = HashMap::new();
    for field in fields.into_inner() {
        let (name, value) = parse_field(field);
        if let Some(v) = result.remove(&name) {
            result.insert(name, format!("{} {}", v, value));
        } else {
            result.insert(name, value);
        }
    }
    result
}

fn write_docs(desc: &Desc, docs: &[HashMap<String, String>]) -> std::io::Result<()> {
    for doc in docs {
        let section = doc.get("section").ok_or(ErrorKind::Other)?;
        let format = doc.get("format").ok_or(ErrorKind::Other)?;
        let text = doc.get("text").ok_or(ErrorKind::Other)?;
        let filename = format!(
            "{}-{}.{}",
            &desc.tool.name,
            section.to_ascii_lowercase().replace(" ", "-"),
            format
        );
        let mut file = File::create(filename)?;

        file.write_all(text.as_bytes())?;
    }
    Ok(())
}

fn write_desc(desc: &Desc) -> std::io::Result<()> {
    let filename = format!("{}-options.json", &desc.tool.name);
    let file = File::create(filename).expect("unable to create options file");
    serde_json::to_writer_pretty(&file, &desc).map_err(|e| e.into())
}

fn main() {
    let opt = Opt::from_args();
    let unparsed_file = std::fs::read_to_string(opt.file).expect("unable to read file");
    let file = AutoGenParser::parse(Rule::file, &unparsed_file)
        .expect("unable to parse")
        .next()
        .unwrap();

    let mut tool = ToolDesc::default();
    let mut sections = Vec::new();
    let mut docs = Vec::new();

    // global section
    sections.push(SectionDesc::default());

    for section in file.into_inner() {
        match section.as_rule() {
            Rule::field => {
                let (name, value) = parse_field(section);
                let section = &mut sections.iter_mut().last().unwrap();
                match name.as_str() {
                    "id" => section.id = Some(value),
                    "desc" => section.description = Some(value),
                    "prog-name" => tool.name = value,
                    "prog-title" => tool.title = value,
                    "prog-desc" => tool.description = value,
                    "short-usage" => tool.short_usage = value,
                    "argument" => tool.argument = value,
                    "reorder-args" => tool.reorder_arguments = Some(true),
                    &_ => (),
                }
            }
            Rule::opt => {
                let mut fields = parse_fields(section);
                if fields.contains_key("documentation") {
                    // rename unusual field names
                    rename_key(&mut fields, "descrip", "description");
                    fields.remove("documentation");
                    if let Some(v) = fields.remove("name") {
                        fields.insert("id".to_string(), v.replace("_", "-"));
                    }
                    sections.push(SectionDesc {
                        id: fields.get("id").map(|s| s.to_string()),
                        description: fields.get("description").map(|s| s.to_string()),
                        options: Vec::new(),
                    });
                } else {
                    let argument_range = if let Some(v) = fields.remove("argument-range") {
                        let v: Vec<_> = v.splitn(2, "->").collect();
                        Some(Range {
                            min: v[0].trim().parse().ok(),
                            max: v[1].trim().parse().ok(),
                        })
                    } else {
                        None
                    };

                    let min: Option<usize> = if let Some(v) = fields.remove("min") {
                        if v == "0" {
                            None
                        } else {
                            Some(v.parse().unwrap())
                        }
                    } else {
                        None
                    };
                    let max: Option<usize> = if let Some(v) = fields.remove("max") {
                        if v == "NOLIMIT" {
                            None
                        } else {
                            Some(v.parse().unwrap())
                        }
                    } else {
                        None
                    };
                    let occur_range = if min.is_some() || max.is_some() {
                        Some(Range { min: min, max: max })
                    } else {
                        None
                    };

                    let argument_type = fields
                        .remove("argument-type")
                        .map(|v| ArgumentType::from(v.as_str()));

                    if let Some(v) = fields.remove("disable") {
                        fields.insert("disable-prefix".to_string(), format!("{}-", v));
                        if let Some(v) = fields.remove("enable") {
                            fields.insert("enable-prefix".to_string(), format!("{}-", v));
                        }
                    }

                    let section = &mut sections.iter_mut().last().unwrap();
                    section.options.push(OptionDesc {
                        long_option: fields.get("name").unwrap().replace("_", "-"),
                        description: fields
                            .get("descrip")
                            .map(|s| s.trim_end_matches('.').to_string()),
                        detail: fields.get("doc").map(|s| s.to_string()),
                        short_option: fields.get("value").map(|s| s.to_string()),
                        aliases: fields.get("aliases").map(|s| s.to_string()),
                        conflicts: fields
                            .get("flags-cant")
                            .map(|s| s.split(" ").map(|v| v.to_string()).collect()),
                        requires: fields
                            .get("flags-must")
                            .map(|s| s.split(" ").map(|v| v.to_string()).collect()),
                        file_exists: fields.get("file-exists").map(|_| true),
                        deprecated: fields.get("deprecated").map(|_| true),
                        argument_range,
                        argument_type,
                        argument_name: fields.get("argument-name").map(|s| s.to_string()),
                        argument_optional: fields.get("argument-optional").map(|_| true),
                        multiple: fields.get("stack-arg").map(|_| true),
                        occur_range,
                        enabled: fields.get("enabled").map(|_| true),
                        disable_prefix: fields.get("disable-prefix").map(|s| s.to_string()),
                        enable_prefix: fields.get("enable-prefix").map(|s| s.to_string()),
                    });
                }
            }
            Rule::doc => {
                let mut fields = parse_fields(section);
                // rename unusual field names
                rename_key(&mut fields, "ds-type", "section");
                rename_key(&mut fields, "ds-format", "format");
                rename_key(&mut fields, "ds-text", "text");
                docs.push(fields);
            }
            _ => {}
        }
    }

    let desc = Desc {
        format_version: "0.1.0".to_string(),
        tool,
        sections,
    };
    write_docs(&desc, &docs).expect("unable to write docs");
    write_desc(&desc).expect("unable to write options");
}
